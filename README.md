### Clone, build and run
```
git clone --recurse-submodules https://gitlab.com/mephi_timetable/mephi_timetable
cd mephi_timetable
mkdir -p ~/h2-data
cd api
./gradlew buildFatJar
cd ../parser
./gradlew distTar
cd ..
docker compose build
docker compose up
```
